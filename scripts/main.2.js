var glob = 1;
glob++;

function increaseGlob() {
    var glob = 1;
    glob++;

    function increaseAgain() {
        glob++;
    }
    increaseAgain();
    console.log('Glob inside', glob);
}
// increaseGlob();

function globIncreaser() {
    var glob = 1;
    glob++;

    function increaseAgain() {
        glob++;
        console.log('Glob inside', glob);
        return glob;
    }
    increaseAgain();
    return increaseAgain;
}

console.log('Glob inside in global', globIncreaser()())
// console.log('Glob', glob);