/* var arr = ['prvni', 2, false];

console.log('array', arr);
console.log('array length', arr.length);

console.log('zero item', arr[0]);
console.log('unknow item', arr[5]);

arr[8] = 'eight';
console.log('longer arr', arr);

console.log('prvni index', arr.indexOf(true));

for (var idx in arr) {
    console.log(arr[idx]);
}

console.log('last removed', arr.pop());
console.log('shorter arr', arr);

arr.push('dalsi');
console.log('longer arr', arr);

arr.shift();
arr.unshift('first');

console.log('arr again', arr);

console.log('sliced', arr.slice(2, 3));
console.log('joined', arr.join(' - '));

var arr2 = [];
arr2.fill(0, 0, 10);
console.log('filled', arr2);

arr.forEach(function(item) {
    console.log('For each', item);
}); */

var numbers = [1, 12, 6, 4];
console.log(numbers);

var greaterThan5 = function(item){
    return item > 5;
};

var numbersIncreased = numbers.filter(greaterThan5).map(function(item, idx){
    return item + ' pos ' + idx;
});
console.log(numbersIncreased);

console.log('some greater than 5', numbers.some(greaterThan5));
console.log('every greater than 5', numbers.every(greaterThan5));

console.log('sorted', numbers.sort());

numbers.sort(function(a, b){
    return a > b;
});
console.log('sorted', numbers);

var second = [7, 9];
console.log('concatenated', numbers.concat(second, [13, 15], 20, 25));

var multi = [[1, 2], [3, 4]];
console.log(multi[1][0]);