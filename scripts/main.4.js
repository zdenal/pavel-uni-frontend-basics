var obj = {
    zero: 0,
    second: 2
};

var obj2 = {
    first: 1,
    second: 'second',
    third: {
        some: 'some',
        arr: [1, 4, 6]
    }
};

console.log('obj', obj, 'obj2', obj2);

for (var key in obj2) {
    console.log(key, obj2[key]);
}

console.log(obj2.third);
obj2.fifth = '42';
obj2['sixth  abcd'] = '1';

console.log(obj2);
console.log(obj2.third.some);

/*var obj3 = Object.assign(obj, obj2);
console.log(obj, obj2, obj === obj3);*/

var objLikeCopy = {
    zero: 0,
    second: 2
};

console.log('equals', obj === objLikeCopy);

var obj4 = {
    getDate: function() {
        return new Date();
    },
    getTimestamp: function(delay, conf) {
        var config = Object.assign({}, conf || {});
        delay && alert('delay: ' + delay);
        return this.getDate().getTime() + (delay || this.delay);
    },
    delay: 5
};

console.log(obj4.getDate());
console.log(obj4.getTimestamp());
// console.log(obj4.getTimestamp(400));

console.log(Object.assign({}, obj, obj2), obj);
