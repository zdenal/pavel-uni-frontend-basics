const same = '42';
// same = '15';

let prop = '42';
prop = '15';

console.log(same, prop, same === prop);

const arr = [1, 2, 3];

arr.map(function(item) {
    return item + 1;
});
arr.map((item) => {
    return item + 1;
});
arr.map((item) => item + 1);
arr.map(item => item + 1);
const arr2 = arr.map(_ => _ + 1);

arr.map((item, idx) => item + 1);

arr.map((item, idx) => {
    return { item: item, idx: idx };
});

console.log('arr2', arr2);

/* const obj = {
    impl: () => {
        const fn = () => {
            console.log('value', this.value);
        };
        fn();
    },
    value: 5
};

obj.impl(); */

const fn = (a, b = 2) => {
    console.log('a', a, 'b', b);
};
fn(1);
fn(1, 6);

const fn2 = (a = 2, b) => {
    console.log('a', a, 'b', b);
};
fn2(6, 12);

const fn3 = (a, b, ...params) => {
    console.log('a', a, 'b', b);
    params.forEach((param) => console.log(param));
};
// fn3(1, 2, 3, 4, 5, 6);

const params = [8, 9, 10];
fn3(1, 2, ...params);

const params2 = [1, ...params, 20];
console.log('params2', params2);

const str = `neco     ${params[0] === 8 ? 'malyho' : 'velkyho'}`;
console.log('str', str);

const x = 1, y = 2;
const obj = { x, y, z: 3 };
console.log('obj', obj);

const list = [ 1, 2, 3 ];
const l1 = list[0];
const l3 = list[2];
let [ i1, , i3 ] = list;
[ i3, i1 ] = [ i1, i3 ];
console.log('i3', i3, 'i1', i1);

const obj2 = {
    a: 1,
    b: 2,
    c: 3,
    d: { 
        x: 42, 
        y: 15 
    } 
};
const fn4 = (obj) => {
    const a = obj.a;
    const { b, c, d: { x } } = obj;
    const { b: b2, c: c2, d: { x: x2 } } = obj;
    console.log(b, c, x, b2, c2, x2);
};
fn4(obj2);

const fn5 = ({ b, c, d: { x } }) => {
    console.log(b, c, x);
};
fn5(obj2);

class Shape {
    constructor() {
        this.val1 = '';
        this.test();
    }
    test() {
        console.log('test');
    }
}