var propInt = 42;
propInt = 43;

var propDouble = 21.5;
var propString = "str 1\"";
var propString2 = 'str 2"';

var propBool = false;

var propUndefined;
var propUndefined2 = undefined;

var propNull = null;

propInt = '456';

while (propInt < 2056) {
    propInt++;
}
console.log('while', propInt);

do {
    propInt--;
} while (propInt !== 1);
console.log('do while', propInt);

/*for (var i = 0; i < 10; i++) {
    console.log('i', i);
}*/


console.log(secondProp);
showText();

function showText() {
    console.log('text');
}

var fnSpecTxt = function(text) {
    console.log('text is', text);
}

fnSpecTxt('něco');

function secondFunc(firstFunc, text) {
    firstFunc(text);
}
secondFunc(fnSpecTxt, 'second text');

var secondProp = 25;
console.log(secondProp);