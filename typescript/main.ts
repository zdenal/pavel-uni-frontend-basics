const str: string = '';
const num: number = 2.0;

console.log(`2 === ${num}`, 2 === num);

const list: number[] = [2, 2.5];

enum Color {
    Red,
    Green,
    Blue
}
const c: Color = Color.Blue;
console.log('color', c);
console.log('color', Color[Color.Blue]);

const all: string = 2 as any;

function test(value: string): void {
    console.log('test', value);
}
test('some');

interface Person {
    firstName: string;
    lastName: string;
    age?: number;
    say?: (word: string) => void;
}

const petr: Person = {
    firstName: 'Petr',
    lastName: 'Pan',
    say: (word: string) => {
        console.log(word);
    }
};

console.log(petr.firstName);
petr.say('Hello');