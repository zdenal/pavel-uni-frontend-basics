export interface Person {
    name: string;
    age: number;
    sex: 'male' | 'female';
}